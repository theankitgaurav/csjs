// Fibonacci sum can be calculated in two ways:
// viz. Recursive and Iterative methods
// While the recursive is easier to grasp for some, it end up wasting a lot of memory and cpu time 
// by following a top-down approach
// On the other hand, the iterative method is more intuitive in the sense that follows Dynamic Programming

// num stores the number whose Fibonacci sum is to be calculated
const num = 5

// Recursive function
function fibo(num){
	if ( num == 1 || num == 2)
		return 1
	else
		return ( fibo( num - 1 ) + fibo( num - 2 ) )
}

// Iterative function
function fibo(num){
	var fibo = []
	fibo[0] = fibo[1] = 1
	for( var i = 2; i < num; i++){
		fibo[i] = fibo[i-1] + fibo[i-2]
	}
	return fibo[num-1]
}
