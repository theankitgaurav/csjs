# Algorithms and Data Structures Implementations in JavaScript (ES6)
Easy-to-understand Implementations of Some Important Algorithms in Javascript

## List of Implementations

+ Stacks
+ Queues
+ Linked Lists
+ Insertion Sort
+ Quick Sort
+ Merge Sort
+ Graph
+ Binary Search Tree

# Contributions
Changes and improvements are more than welcome!

Feel free to fork and open a pull request.
Please make your changes in a specific branch and request to pull into master!
